class DeezFile {
    constructor(json) {
        this.name = json.SNG_TITLE.replace(/\.mp3$/, '');
        this.id = json.SNG_ID.toString();
        this.size = parseInt(json.FILESIZE_MP3_MISC.toString(), 10);
        this.md5origin = json.MD5_ORIGIN;
        this.calculateUrlPart();
    }

    //Calculate download url part
    calculateUrlPart() {
        let md5 = this.md5origin.replace('.mp3', '');
        let isExt = this.md5origin.includes('.mp3') ? '1' : '0';
        this.urlPart = `${md5}${isExt}0${this.id}`;
    }
}

class MultiFile {
    //Raw naming format: $[aaabbb]name
    //aaa - id
    //bbb - part

    //Initial JSON
    constructor(json) {
        this.name = this.parseName(json.SNG_TITLE);
        this.id = json.SNG_TITLE.substring(2, 5);
        this.parts = [];
        this.size = 0;
    }

    //Get id used for searching
    static getId(json) {
        return json.SNG_TITLE.substring(2, 5);
    }

    parsePart(json) {
        let p = new MultiFilePart(json);
        this.size += p.size;
        this.parts.push(p);
    }

    //Get clean filename
    parseName(n) {
        return n.replace(/^\$\[......\]/, '').replace(/\.mp3$/, '');
    }

    //Should be called after all parts are inserted
    calculateUrlPart() {
        //Sort parts
        //Part ID = index
        this.parts = this.parts.sort((a, b) => (a.id > b.id) ? 1 : -1);
        
        //Calculate download url part, _ as a separator
        let urlPart = '';
        for(let part of this.parts) {
            let md5 = part.md5origin.replace('.mp3', '');
            let isExt = part.md5origin.includes('.mp3') ? '1' : '0';
            urlPart += `${md5}${isExt}0${part.did}_`;
        }

        //Get rid of the last separator
        this.urlPart = urlPart.slice(0, -1);
    }
}

class MultiFilePart {
    constructor(json) {
        this.md5origin = json.MD5_ORIGIN;
        //id = part id/number
        //did = deezer id
        this.id = parseInt(json.SNG_TITLE.substring(5, 8), 16);
        this.did = json.SNG_ID;
        this.size = parseInt(json.FILESIZE_MP3_MISC.toString(), 10);
    }
}

//Actual user uploaded MP3
class Song {
    constructor(json) {
        this.name = json.SNG_TITLE.replace(/\.mp3$/, '');
        this.id = json.SNG_ID.toString();
        this.size = parseInt(json.FILESIZE_MP3_MISC.toString(), 10);
        this.md5origin = json.MD5_ORIGIN;
        this.artist = json.ART_NAME;
        this.album = json.ALB_TITLE;
        this.duration = parseInt(json.DURATION.toString(), 10);
        this.thumb = `https://e-cdns-images.dzcdn.net/images/cover/${json.ALB_PICTURE}/100x100-000000-80-0-0.jpg`
        this.calculateUrlPart();
    }

    //Calculate download url part
    calculateUrlPart() {
        let md5 = this.md5origin.replace('.mp3', '');
        let isExt = this.md5origin.includes('.mp3') ? '1' : '0';
        this.urlPart = `${md5}${isExt}0${this.id}`;
    }
}

class FileParser {
    static parseFiles(json) {
        let root = json.TAB.personal_song.data;

        let out = {
            files: [],
            multifiles: [],
            songs: [],
            info: {
                count: 0,
                size: 0,
                lastId: 0,
                freeSpace: 2000 * 200 * 1000000 //2000 Tracks, 200MB each
            }
        }
        for(let j of root) {
            //Save meta
            out.info.count++;
            let filesize = parseInt(j.FILESIZE_MP3_MISC.toString(), 10);
            out.info.size += filesize;
            //Space available = n tracks, not MB. 
            out.info.freeSpace -= 200*1000000;

            //Check if multifile
            if (/^\$\[......\]/.test(j.SNG_TITLE)) {
                let id = MultiFile.getId(j);
                let res = out.multifiles.filter((f) => f.id == id);
                //Existing multifile
                if (res.length > 0) {
                    out.multifiles[out.multifiles.indexOf(res[0])].parsePart(j);
                //New
                } else {
                    let f = new MultiFile(j);
                    f.parsePart(j);
                    out.multifiles.push(f);

                    //Save highest multifile ID.
                    if (f.id > out.info.lastId) out.info.lastId = f.id;
                }

            } else {
                //Actual song - has duration & artist
                if (parseInt(j.DURATION.toString(), 10) && 
                j.ART_NAME && j.ART_NAME.length > 0) {

                    out.songs.push(new Song(j));
                //Normal file
                } else {
                    out.files.push(new DeezFile(j));
                }
                
            } 
        }

        //Fix MultiFiles
        for(let i=0; i<out.multifiles.length; i++) {
            out.multifiles[i].calculateUrlPart();
        }
    
        return out;
    }
}

module.exports = {DeezFile, FileParser};