const axios = require('axios');
const crypto = require('crypto');
const https = require('https');
const querystring = require('querystring');
const randomstring = require("randomstring");
const {Transform, Writable, Readable} = require('stream');
const { resolve } = require('path');

class DeezerAPI {
    constructor(arl, options = {}) {
        this.url = 'https://www.deezer.com/ajax/gw-light.php';
        this.arl = arl;

        //Preauthorized
        this.sid = options.sid;
        this.token = options.token;
        this.userId = options.userId;
        this.username = options.username;
    }

    //Get headers
    headers() {
        let cookie = `arl=${this.arl}`;
        if (this.sid) cookie += `; sid=${this.sid}`;
        return {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
            "Content-Language": "en-US",
            "Cache-Control": "max-age=0",
            "Accept": "*/*",
            "Accept-Charset": "utf-8,ISO-8859-1;q=0.7,*;q=0.3",
            "Accept-Language": "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7",
            "Connection": 'keep-alive',
            "Cookie": cookie
        }
    }

    //Call gw-light API
    async callAPI(method, args = {}) {
        let data = await axios({
            url: this.url,
            method: 'POST',
            headers: this.headers(),
            responseType: 'json',
            params: {
                api_version: '1.0',
                api_token: this.token ? this.token : 'null',
                input: '3',
                method: method,
            },
            data: args
        });

        //Save SID cookie to not get token error
        if (method == 'deezer.getUserData') {
            let sidCookie = data.headers['set-cookie'].filter((e) => e.startsWith('sid='));
            if (sidCookie.length > 0) {
                sidCookie = sidCookie[0].split(';')[0];
                this.sid = sidCookie.replace('sid=', '');
            }
        }

        return data.data;
    }

    //Authorize using arl && load user data
    async authorize() {
        let data = await this.callAPI('deezer.getUserData');
        this.token = data.results.checkForm;
        this.userId = data.results.USER.USER_ID;
        this.username = data.results.USER.BLOG_NAME;

        if (!this.userId || !this.token || this.userId == 0) return false;
        return true;
    }

    //Get Deezer CDN track url
    static getURL(trackId, md5origin, mediaVersion, quality = 3) {
        const magic = Buffer.from([0xa4]);

        let step1 = Buffer.concat([
            Buffer.from(md5origin),
            magic,
            Buffer.from(quality.toString()),
            magic,
            Buffer.from(trackId),
            magic,
            Buffer.from(mediaVersion.toString())
        ]);
        //MD5
        let md5sum = crypto.createHash('md5');
        md5sum.update(step1);
        let step1md5 = md5sum.digest('hex');

        let step2 = Buffer.concat([
            Buffer.from(step1md5),
            magic,
            step1,
            magic
        ]);
        //Padding
        while(step2.length%16 > 0) {
            step2 = Buffer.concat([step2, Buffer.from('.')]);
        }
        //AES
        let aesCipher = crypto.createCipheriv('aes-128-ecb', Buffer.from('jo6aey6haid2Teih'), Buffer.from(''));
        let step3 = Buffer.concat([aesCipher.update(step2, 'binary'), aesCipher.final()]).toString('hex').toLowerCase();

        return `https://e-cdns-proxy-${md5origin.substring(0, 1)}.dzcdn.net/mobile/1/${step3}`;
    }
}


class DeezerDecryptionStream extends Transform {

    constructor(trackId, options = {offset: 0}) {
        super();
        this.key = this.getKey(trackId)
        //Offset as n chunks
        this.offset = Math.floor(options.offset / 2048);
        //How many bytes to drop
        this.drop = options.offset % 2048;
        this.buffer = Buffer.alloc(0);
    }

    _transform(chunk, encoding, next) {
        //Restore leftovers
        chunk = Buffer.concat([this.buffer, chunk]);

        while (chunk.length >= 2048) {
            //Decrypt
            let slice = chunk.slice(0, 2048);
            if ((this.offset % 3) == 0) {
                slice = this.decryptChunk(slice);
            }
            this.offset++;

            //Cut bytes
            if (this.drop > 0) {
                slice = slice.slice(this.drop);
                this.drop = 0;
            }
            
            this.push(slice);

            //Replace original buffer
            chunk = chunk.slice(2048);
        }
        //Save leftovers
        this.buffer = chunk;
        
        next();
    }

    //Last chunk
    async _flush(cb) {
        //drop should be 0, so it shouldnt affect
        this.push(this.buffer.slice(this.drop));
        this.drop = 0;
        this.buffer = Buffer.alloc(0);
        cb();
    }

    getKey(trackId) {
        let secret = "g4el58wc0zvf9na1";
        //MD5 TrackId
        let md5 = crypto.createHash('md5');
        md5.update(trackId);
        let md5id = md5.digest('hex');
        //Make key
        let key = '';
        for(let i=0; i<16; i++) {
            key += String.fromCharCode([md5id.charCodeAt(i) ^ md5id.charCodeAt(i+16) ^ secret.charCodeAt(i)]);
        }
        return key;
    }

    //Decrypt 2048b chunk
    decryptChunk(data) {
        const bf = crypto.createDecipheriv('bf-cbc', this.key, Buffer.from([0,1,2,3,4,5,6,7]));
        bf.setAutoPadding(false);
        let dec = bf.update(data);
        return Buffer.concat([dec, bf.final()]);
    }
}

class DeezerUploadStream extends Writable {
    //deezer = instance of DeezerAPI, AUTHORIZED!!
    //name = filename to upload
    //estSize = estimated size (<200MB or more)
    //fileId = used for >200MB split files, should be 3 characters long
    constructor(deezer, name, estSize, fileId = 0) {
        super();
        this.CHUNKSIZE = 200*1000*1000 //200MB
        
        this.deezer = deezer;
        this.name = name;
        this.estSize = estSize;
        this.fileId = fileId.toString(16).padStart(3, '0');
        //FileID has to be 3 characters long
        if (this.fileId.length > 3) this.fileId = this.fileId.substring(0, 3);

        this.filename;
        //Leftovers
        this.buffer = new Buffer.alloc(0);
        //Deezer friendly
        this.part = 0;
        this.uploaded = 0;
        //Contains track ids
        this.output = [];
        this.req;
    }

    //Create POST multipart form upload request to Deezer
    async _createRequest() {
        //Generate filename
        let filename = this.name + '.mp3';
        if (this.estSize > this.CHUNKSIZE) {
            //Part as HEX, because 3 digits, has to be max 2000.
            let part = this.part.toString('16').padStart(3, '0');
            filename = `$[${this.fileId}${part}]${filename}`;
        }

        let url = 'https://upload.deezer.com/?' + querystring.stringify({
            sid: this.deezer.sid,
            id: this.deezer.userId,
            resize: 0,
            directory: 'user',
            type: 'audio',
            referer: 'FR',
            file: filename
        });
        this.filename = filename;

        //Multipart request boundary
        this.boundary = '---6969deeznuts' + randomstring.generate(16);

        this.req = https.request(url, {
            method: 'POST',
            headers: {
                'Content-Type': `multipart/form-data;boundary="${this.boundary}"`,
                //Not required
                //'Content-Length': ''
            }
        }, (res) => {
            //Parse deezer response
            res.on('data', async (d) => {
                let res = JSON.parse(d.toString('utf-8'));
                let id = res.results.toString();
                this.output.push(id);

                //If multifile - force rename, to prevent deezer parsing the tags
                if (this.estSize > this.CHUNKSIZE) {
                    let res = await this.deezer.callAPI('personal_song.update', {
                        data: {
                            SNG_TITLE: this.filename,
                            ALB_TITLE: '',
                            ART_NAME: ''
                        },
                        upload_id: id
                    });
                }

                this.onRequestEnd();
            })
        });

        //Write multipart header
        let header = `--${this.boundary}\r\nContent-Disposition: form-data; name="file"; filename="${filename}"\r\nContent-Type: audio/mpeg\r\n\r\n`;
        if (!this.req.write(header)) await this._waitWrite();
    }

    _endRequest() {
        //Write footer
        this.req.write(`\r\n--${this.boundary}--\r\n`);
        this.req.end();
    }

    //Wait for https req.write
    async _waitWrite() {
        await new Promise((resolve, rej) => {
            this.req.once('drain', () => resolve());
        });
    }

    async _write(chunk, enc, next) {
        if (!this.req) await this._createRequest();
        //Restore leftovers
        chunk = Buffer.concat([this.buffer, chunk]);
        this.buffer = Buffer.alloc(0);

        //Split files
        if ((chunk.length + this.uploaded) > this.CHUNKSIZE) {
            let offset = this.CHUNKSIZE - this.uploaded;
            //Send
            if (!this.req.write(chunk.slice(0, offset))) await this._waitWrite();
            //Save leftovers
            this.buffer = chunk.slice(offset);
            //Await deezer response
            await new Promise((res, rej) => {
                this.onRequestEnd = res;
                this._endRequest();
            });
            //Cleanup
            this.req = null;
            this.part++;
            this.uploaded = 0;
            return next();
        }

        //Send
        if (!this.req.write(chunk)) await this._waitWrite();
        this.uploaded += chunk.length;

        next();
    }

    async _final(cb) {
        if (!this.req.write(this.buffer)) await this._waitWrite();
        //Wait for response from deezer
        await new Promise((res, rej) => {
            this.onRequestEnd = res;
            this._endRequest();
        });
        cb();
    }
}

class DeezerDownloadStream extends Readable {
    constructor(info, filename, offset) {
        super();
        this.CHUNKSIZE = 200*1000*1000; //200MB

        this.parseInfo(info);
        this.filename = filename;
        
        this.offset = offset;
        this.fileIndex = 0;
        //Files can be slightly above 200MB, hence the length check
        if (offset > this.CHUNKSIZE && this.files.length > 0) {
            this.fileIndex = Math.floor(this.offset / this.CHUNKSIZE);
            this.offset = offset % this.CHUNKSIZE;
        }

        this.ended = false;
        this.canRead = false;
        this.decryptor;
        this.request;
    } 

    //Start & return size
    async start(exactSize = true) {
        let size = 0;
        //If multifile, get size of last chunk
        if (this.files.length > 1 && exactSize) {
            let r = await axios.head(this.files[this.files.length - 1].getUrl());
            size = (this.files.length - 1) * this.CHUNKSIZE + parseInt(r.headers['content-length'], 10);
        }
        
        //Round to 2048 for decryption
        let deezStart = this.offset - (this.offset % 2048);
        this.decryptor = new DeezerDecryptionStream(this.files[this.fileIndex].trackId, {offset: this.offset});
        this.offset = 0;

        //Decryptor
        this.decryptor.on('end', () => {
            this.ended = true;
        });

        //Get data from cdn
        await new Promise((resolve, reject) => {
            let url = this.files[this.fileIndex].getUrl();
            this.request = https.get(url, {
                headers: {'Range': `bytes=${deezStart}-`}
            }, (res) => {
                //Get single file size
                if (size == 0 && exactSize) size = parseInt(res.headers["content-length"]) + deezStart;
                res.pipe(this.decryptor);
                resolve();
            });
        });
        
        return size;
    }
    
    //Parse `f` query parameter
    //_ is used as a separator for multiple files
    parseInfo(info) {
        this.files = info.split('_').map((f) => new DownloadParams(f));
    }

    //Passthru
    async _read(n) {
        //Decryptor ended
        if (this.ended) {
            //Last file
            if (this.fileIndex == this.files.length - 1) {
                this.push(null);
                return;
            //Load next file
            } else {
                this.ended = false;
                this.fileIndex++;
                await this.start(false);
            }
        }

        //Wait for readable
        this.decryptor.once('readable', () => {
            //Passthru data 
            let data = this.decryptor.read();
            this.push(data);
        });
    }
    
}

//0 - 32 bytes = MD5_ORIGIN, .mp3 removed
//32 = 0 / 1 - if MD5_ORIGIN ends with .mp3
//33 = MEDIA_VERSION
//34 - end = SNG_ID
class DownloadParams {
    constructor(i) {
        this.md5origin = i.substring(0, 32);
        if (i.charAt(32) == '1') this.md5origin += '.mp3';
        this.mediaVersion = parseInt(i.charAt(33));
        this.trackId = i.substring(34);
    }

    getUrl() {
        return DeezerAPI.getURL(this.trackId, this.md5origin, this.mediaVersion, 3);
    }
}

module.exports = {DeezerAPI, DeezerDecryptionStream, DeezerUploadStream, DeezerDownloadStream};