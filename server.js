const express = require('express');
const path = require('path');
const https = require('https');
const mime = require('mime-types');
const Busboy = require('busboy');
const { DeezerAPI, DeezerUploadStream, DeezerDownloadStream } = require('./src/deezer');
const { FileParser } = require('./src/definitions');

//Express config
const app = express();
const port = 16969;
app.use(express.static(path.join(__dirname, 'client', 'dist')));
app.use(express.json());

//Post arl as body
app.post('/login', async (req, res) => {
    let arl = req.body.arl;
    if (!arl) return res.status(500).send('Missing ARL');

    //Login to deezer
    let deezer = new DeezerAPI(arl);
    if (!(await deezer.authorize)) return res.status(500).send('Invalid/Expired ARL');

    //Response
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        arl: arl
    }));
});

//Parse ARL from cookie
function parseAuth(req) {
    let arl = req.headers.cookie.split('; ').filter((e) => e.startsWith('arl='))[0];
    arl = arl.replace('arl=', '');
    return new DeezerAPI(arl);
}

//Get ""files"" from deezer
app.get('/files', async (req, res) => {
    let deezer = parseAuth(req);
    await deezer.authorize();

    //Get data from API
    let data = await deezer.callAPI('deezer.pageProfile', {
        nb: 200,
        tab: 'personal_song',
        user_id: deezer.userId
    });
    
    //Error
    if (!data.results || data.error.length > 0) return res.status(500).send('Error loading data, check ARL!');

    let files = FileParser.parseFiles(data.results);
    //Inject additional info
    files.info.username = deezer.username;

    //Response
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Cache-Control', 'no-store');
    res.end(JSON.stringify(files));
});

//Get file
app.get('/get', async (req, res) => {
    //f - download info, n - filename
    let info = req.query.f;
    let filename = req.query.n;

    //Ranged headers
    let range = 'bytes=0-';
    if (req.headers.range) range = req.headers.range;
    let rangeParts = range.replace(/bytes=/, '').split('-');
    let start = parseInt(rangeParts[0], 10);
    let end = '';
    if (rangeParts.length >= 2) end = rangeParts[1];
    //Start stream
    let downStream = new DeezerDownloadStream(info, filename, start);
    let size = await downStream.start();

    //Fix request end bytes
    if (!end || end == '') end = (size - 1).toString();

    //Shared headers
    let headers = {
        'Content-Type': mime.contentType(filename) ? mime.contentType(filename) : 'application/octet-stream',
        'Content-Disposition': `attachment; filename="${encodeURIComponent(filename)}"`
    }
    //Ranged response
    if (req.headers.range) {
        res.writeHead(206, {
            'Content-Range': `bytes ${start}-${end}/${size}`,
            'Content-Length': size - start,
            'Accept-Ranges': 'bytes',
            ...headers
        });
    //Normal response
    } else {
        res.writeHead(200, {
            'Content-Length': size,
            ...headers
        });
    }

    downStream.pipe(res);
});

app.post('/upload', async (req, res) => {
    //Authorize to get SID & UserID
    let deezer = parseAuth(req);
    if (!(await deezer.authorize())) return res.status(500).send('Unable to authorize!');

    //Multifile id
    let mfid = req.query.id;
    if (!mfid) mfid = Math.ceil((Math.random() * 500) + 500);

    //Busboy to parse multipart form
    let busboy = new Busboy({headers: req.headers});
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        //Pipe file to uploader
        let uploader = new DeezerUploadStream(
            deezer,
            filename, 
            //-100, because rough header size, exact value doesn't matter because max error is ~30MB
            parseInt(req.headers['content-length'].toString(), 10) - 100,
            mfid
        );
        file.pipe(uploader);
    });
    busboy.on('finish', () => {
        //Response on done
        res.writeHead(200, {'Connection': 'close'});
        res.end('Upload finished.');
    });
    req.pipe(busboy);
});

//Post JSON array with song ids to delete
app.post('/delete', async (req, res) => {
    //Authorize
    let deezer = parseAuth(req);
    if (!(await deezer.authorize())) return res.status(500).send('Unable to authorize!');

    //Delete requests
    for (let id of req.body) {
        await deezer.callAPI('personal_song.delete', {
            upload_id: parseInt(id, 10)
        });
    }

    res.status(200).send('OK');
});

//Start
app.listen(port, () => {
    console.log(`Running on port: ${port}`);
});