import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import axios from 'axios';
import VueCookie from 'vue-cookie';
import Files from './views/Files.vue';

//Routing
const routes = {
    '/': App,
    '/files': Files
}

//404 page
const NotFound = {template: '<h1>Invalid path!</h1>'};

//Global axios
Vue.prototype.$axios = axios.create({
    baseURL: window.location.origin
});

//Filesize formatter (https://www.codexworld.com/how-to/convert-file-size-bytes-kb-mb-gb-javascript/)
function _filesize(bytes,decimalPoint) {
    if (bytes == 0) return '0 B';
    var k = 1000,
        dm = decimalPoint || 2,
        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
Vue.prototype.$filesize = _filesize;

//Duration formatter (https://gist.github.com/vankasteelj/74ab7793133f4b257ea3)
function _duration(time) {
    let pad = function(num, size) { return ('000' + num).slice(size * -1); };
    let minutes = Math.floor(time / 60) % 60;
    let seconds = Math.floor(time - minutes * 60);
    return pad(minutes, 2) + ':' + pad(seconds, 2);
}
Vue.prototype.$duration = _duration;

//Cookies
Vue.use(VueCookie);


Vue.config.productionTip = false
new Vue({
    data: {
        //Routing without router
        currentRoute: window.location.pathname,

        auth: {
            arl: null,
        }
    },
    computed: {
        CurrentPage() {
            return routes[this.currentRoute] || NotFound
        }
    },
    mounted() {
        this.auth.arl = this.$cookie.get('arl');
    },
    vuetify,
    render(h) { return h(this.CurrentPage) }
}).$mount('#app');
