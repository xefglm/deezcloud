# Deezcloud

Deezer user uploaded MP3s as a cloud storage.

## Demo

Deezcloud is currently hosted at: https://cloud.shibe.rocks

## Installing

### Dependencies:

NodeJS: https://nodejs.org/en/download/

### Frontend

```
cd client/
npm install
npm run build
```

This will put the generated files in `client/dist` directory, which is also used as static dir for the server.

### Server:

```
npm install
node server.js
```

This will start the server on port `16969`, you can modify it by changing top lines in `server.js`

### Reverse proxy / Cloudflare DNS IMPORTANT:

If you plan to use this with reverse proxy or Cloudflare DNS, you might get `413` error while uploading.
This is caused by limiting the maximum upload body size. You can fix this in `nginx` by adding `client_max_body_size 0;` to the config.

### Info

#### Limits:
Deezer limits each song to be max 200MB, and max 2000 songs (=400GB).
If you upload any file above 200MB it will be automatically split during upload and joined while downloading.   

#### Streaming:
Downloading supports `Range` HTTP headers, so streaming should be possible.

### Credits:

**Francesco** - logo, design, tester  
**uhwot** - idea